<?php

class Mgcs_DefaultLimit_Model_Observer
{
    const XML_PATH_DEFAULT_LIMIT = 'admin/grid/default_limit';

    public function setDefaultLimit(Varien_Event_Observer $observer)
    {
        $block = $observer->getEvent()->getBlock();

        if ($block instanceof Mage_Adminhtml_Block_Widget_Grid && !($block instanceof Mage_Adminhtml_Block_Dashboard_Grid)) {
            // unfortunately there's no getDefaultLimit(), otherwise we'd be able to check whether another default was
            // set during block construction (like in the dashboard grid)
            if ($defaultLimit = Mage::getStoreConfig(self::XML_PATH_DEFAULT_LIMIT)) {
                $block->setDefaultLimit($defaultLimit);
            }
        }
    }

}