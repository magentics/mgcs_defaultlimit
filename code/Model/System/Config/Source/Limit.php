<?php

class Mgcs_DefaultLimit_Model_System_Config_Source_Limit
{

    public function toOptionArray()
    {
        return array(
            array('value' => 20, 'label' => 20),
            array('value' => 30, 'label' => 30),
            array('value' => 50, 'label' => 50),
            array('value' => 100, 'label' => 100),
            array('value' => 200, 'label' => 200),
        );
    }

}
